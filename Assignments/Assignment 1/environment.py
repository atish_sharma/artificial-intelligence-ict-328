import time

class environment:

	def __init__(self,env):
		self.area={'A':{'dirty_since':0,'clean':True,'dirty_time':0},'B':{'dirty_since':0,'clean':True,'dirty_time':0}}
		# dirty_time is the duration of time for which an area has remained dirty
		# dirty_time is used to measure the performance of the agent
		# lower the dirty_time, better the performance
		input=open(env)
		lines=input.read().split('\n')
		input.close()
		self.env=[]
		for i in lines:
			line=i.split(',')
			temp=[]
			for j in line:
				temp.append(j.strip())
			temp[0]=int(temp[0])
			self.env.append(temp)
		self.env.sort(key=lambda a:a[0])

	def is_clean(self,pos):
		return self.area[pos]['clean']

	def clean(self,pos):
		if not self.area[pos]['clean']:
			self.area[pos]['dirty_time']+=self.curr_time()-self.area[pos]['dirty_since']
			self.area[pos]['clean']=True
		print('*** Area "{0}" cleaned at "{1}" milliseconds.'.format(pos,self.curr_time()-self.start))

	def dirty(self,pos):
		if self.area[pos]['clean']:
			self.area[pos]['dirty_since']=self.curr_time()
			self.area[pos]['clean']=False
		print('--- Area "{0}" dirtied at "{1}" milliseconds.'.format(pos,self.curr_time()-self.start))

	def curr_time(self):
		return int(round(time.time()*1000))

	def is_simulation_over(self):
		return self.simulation_over

	def simulate(self):
		self.simulation_over=False
		self.start=self.curr_time()
		while True:
			for i in self.env:
				diff=self.curr_time()-self.start
				if i[0]<=diff:
					self.dirty(i[1])
					self.env.remove(i)
				else:
					break
			if len(self.env)==0:
				break
		self.simulation_over=True

	def performance(self):
		print('-------------------')
		print('PERFORMANCE MEASURE')
		print('-------------------')
		print('Performance in this environment is measured by the duration of time for which an area has been left dirty. The less the better.')
		score=0.0
		for key,val in self.area.items():
			score+=val['dirty_time']
			print('Area "{0}" was left dirty for "{1}" milliseconds.'.format(key,val['dirty_time']))
		print('Average score : {0}'.format(score/len(self.area)))



