class agent:

	def __init__(self,env,initial_location):
		self.env=env
		self.loc=initial_location

	def simulate(self):
		while True:
			found_unclean_area=not self.env.is_simulation_over()
			for key,value in self.env.area.items():
				if not self.env.is_clean(key):
					if self.loc!=key:
						print('### Agent moving from "{0}" to "{1}".'.format(self.loc,key))
						self.loc=key
					self.env.clean(self.loc)
					found_unclean_area=True
			if not found_unclean_area:
				break
		self.env.performance()