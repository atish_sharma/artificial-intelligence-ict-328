import sys,threading
from agent import agent
from environment import environment

file_name=sys.argv[1]
init_pos=sys.argv[2]

env=environment(file_name)
robot=agent(env,init_pos)

try:
	threading.Thread(target=env.simulate).start()
	threading.Thread(target=robot.simulate).start()
except:
   print "Error: unable to start thread"