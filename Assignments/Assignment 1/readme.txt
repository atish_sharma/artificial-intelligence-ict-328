The test program instantiates an agent and the environment and runs them concurrently through multithreading.

Input to the environment is given through a file which has comma separated fields (csv). The file can contain multiple rows with 2 columns in each row. The first column represents the time in milliseconds (long integer) from the start of the program. The second column contains the area to be dirtied (either 'A' or 'B').

The agent is provided with the environment and the initial starting position of the agent (either 'A' or 'B').

The test program is provided the file for environment configuration and initial position of the agent through command line arguments, 
e.g.: python test.py environment_data.txt B
SYNTAX : python test.py [environment_configuration_file_location] [initial_position_of_agent]