import sys,pprint,copy

board=[[0,1,0,1,0,1,0,1],
	[1,0,1,0,1,0,1,0],
	[0,1,0,1,0,1,0,1],
	[0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0],
	[2,0,2,0,2,0,2,0],
	[0,2,0,2,0,2,0,2],
	[2,0,2,0,2,0,2,0]
]

it=1

def get_piece_pos(player):
	pos=[]
	for i in range(len(board)):
		for j in range(len(board[i])):
			if board[i][j]==player:
				pos.append([i,j])
	return pos

def next_steps(player,pos):
	steps=[]
	val=[[1,-1],[1,1],[-1,-1],[-1,1]]
	for i in val:
		try:
			p=board[pos[0]+i[0]][pos[1]+i[1]]
			if p!=player:
				steps.append([pos[0]+i[0],pos[1]+i[1]])
		except:
			pass
	return steps

def transition(player,curr,next):
	opp=1
	if player==1:
		opp=2
	b=copy.deepcopy(board)
	b[curr[0]][curr[1]]=0
	b[next[0]][next[1]]=player
	w=0
	for i in b:
		for j  in i:
			if j==opp:
				w-=1
			elif j==player:
				w+=1
	return w


def eval_best_next_step(player,pos):
	eval_val=0
	next_step=None
	ns=next_steps(player,pos)
	for j in ns:
		te=transition(player,pos,j)
		if next_step==None or te>eval_val:
			eval_val=te
			next_step=j
	return eval_val,next_step

def eval(player):
	print '-'*100
	print 'Turn',it,' -> Player',player
	pprint.pprint(board)
	pos=get_piece_pos(player)
	if len(pos)==0:
		print 'Game Over'
		if player==1:
			player=2
		else:
			player=1
		print 'Player',player,'won!'
		return  False
	eval_val=0
	next_step=None
	curr_val=None
	for i in pos:
		e,n=eval_best_next_step(player,i)
		if next_step==None or e>eval_val:
			curr_val=i
			next_step=n
	board[curr_val[0]][curr_val[1]]=0
	board[next_step[0]][next_step[1]]=player
	return True

players=[1,2]
curr_player=players[int(sys.argv[1])-1]

while eval(curr_player):
	it+=1
	curr_player=players[curr_player%len(players)]





