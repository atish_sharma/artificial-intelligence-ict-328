import math,random

class random_agent:

	def __init__ (self,world,pos) :
		self.world = world
		self.pos = pos
		self.path = []
		self.dirtyTiles = self.world.getDirtyTiles()

	def clean(self) :
		while len(self.dirtyTiles) > 0 :
			print('>>> Agent on tile [ {}, {} ]'.format(self.pos[0],self.pos[1]))
			if not self.world.isClean(self.pos):
				self.world.clean(self.pos)
				d = 0
				for i in self.dirtyTiles :
					if i[0] == self.pos[0] and i[1] == self.pos[1]:
						break
					d += 1
				del self.dirtyTiles[d]
			self.path.append(self.pos)
			if len(self.dirtyTiles) == 0 :
				break
			self.pos = self.getNextTile()
		print('\nWorld clean!\n')
		self.performance()

	def getNextTile(self) :
		nextStep = [ [-1,0], [-1,1], [0,1], [1,1], [1,0], [1,-1], [0,-1], [-1,-1]]
		nextTile = []
		for i in nextStep :
			newTile = [ self.pos[0] + i[0], self.pos[1] + i[1]]
			if newTile[0] >= 0  and newTile[0] < self.world.height and newTile[1] >= 0 and newTile[1] < self.world.width :
				nextTile.append(newTile)
		return nextTile[random.randint(0,len(nextTile)-1)]

	def distance(self,a,b) :
		return math.sqrt(math.pow(math.fabs(a[0]-b[0]) + math.fabs(a[1]-b[1]),2)) 

	def performance(self):
		p = 0.0
		if len(self.path) > 0 :
			prev = self.path[0]
			del self.path[0]
			for i in self.path :
				p += self.distance(prev,i)
				prev = i
		print('Performance\nPath Cost : {} units distance traversed\nSearch Cost : {} units distance searched'.format(p,p))
		print('NOTE : path cost and search cost will always be the same here as this algorithm traverses the same path along which it searched for dirty tiles.')

