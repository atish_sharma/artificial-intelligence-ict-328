import sys,ast,world,smart_agent,random_agent

world_data = ast.literal_eval(open(sys.argv[1]).read().strip())
world = world.world(world_data)
if sys.argv[4] == 's' :
	smart_agent = smart_agent.smart_agent(world,[ int( sys.argv[2] ), int( sys.argv[3] ) ] )
	smart_agent.clean()
elif sys.argv[4] == 'r' :
	random_agent = random_agent.random_agent(world,[ int( sys.argv[2] ), int( sys.argv[3] ) ] )
	random_agent.clean()

