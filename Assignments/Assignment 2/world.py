import random

class world:

	def __init__ (self,data) :
		self.randomDirt = data['randomDirt']
		if self.randomDirt :
			self.dirtProbability = data['dirtProbability']
		else :
			self.dirtyTiles = data['dirtyTiles']
		self.height = data['height']
		self.width = data['width']
		self.world = []
		self.generateWorld()

	def generateWorld(self):
		if self.randomDirt :
			self.generateRandomWorld()
		else :
			self.generateGivenWorld()
		self.printWorld()

	def generateRandomWorld(self):
		self.world = [ [ True for i in range(self.width) ] for j in range(self.height) ]
		for i in range(len(self.world)) :
			for j in range(len(self.world[i])) :
				self.world[i][j] = not self.generateDirt()

	def generateGivenWorld(self):
		self.world = [ [ True for i in range(self.width) ] for j in range(self.height) ]
		for i in self.dirtyTiles :
			self.world[i[0]][i[1]] = False

	def getDirtyTiles(self):
		dirtyTiles = []
		for i in range(len(self.world)) :
			for j in range(len(self.world[i])) :
				if not self.world[i][j] :
					dirtyTiles.append( [i,j] )
		return dirtyTiles

	def clean(self,pos):
		i, j = pos[0], pos[1]
		if not self.world[i][j] :
			self.world[i][j] = True
			print('--- Cleaned [ {}, {} ]'.format(i,j))
			self.printWorld()

	def isClean(self,pos):
		return self.world[pos[0]][pos[1]]

	def generateDirt(self):
		return random.randint(1,100) <= self.dirtProbability * 100 

	def printWorld(self):
		print('+++ World [ True <- Clean, False <- Dirty ]')
		for i in self.world :
			print(i)

